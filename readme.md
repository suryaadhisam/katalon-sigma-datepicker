# Katalon Sigma Datepicker

Sigma Datepicker adalah component khusus yang dibuat oleh sigma dan berfungsi untuk memilih tanggal. Component ini bisa ditemukan penggunaannya pada beberapa projek sigma yang dikembangkan saat tulisan ini dibuat.

## Why Do We Have to Use Custom Keyword?
Karena spy biasa belum bisa digunakan untuk menangani komponen tersebut.

## Installation

Pertama-tama silahkan unduh custom keyword untuk sigma datepicker [disini](https://gitlab.com/suryaadhisam/katalon-sigma-datepicker/tree/master/telkomsigma). Proses instalasinya adalah sebagai berikut:
1. Buka Katalon Studio
2. Cari **Keywords** pada *Tests Explorer* projek yang sedang aktif
3. Klik kanan **import**. Pilih folder
4. Silahkan dicari folder dari *custom keyword* yang sudah diunduh sebelumnya
5. Klik **open**

Setelah proses tersebut dilakukan maka custom keyword akan muncul dan siap digunakan.

## Usage
1. Pada *test case* silahkan **add** dengan tipe *custom keyword*

![Image 1](img/1.png)

2. Pilih *custom keyword* dari list yang ada. Dalam hal ini pilih *setDatePicker*

![Image 2](img/2.png)

3. Pada *test case > object* yang dipilih perlu diperhatikan beberapa hal. 

![Image 3](img/3.png)

Pada *object repository* perhatikan *xpath:attributes*. Defaultnya akan memiliki value 
```katalon
a[@id='xxx']
```
Pada kasus projek Askrindo, *id* diatas memiliki value yang tidak tetap alias random. Ketika katalon studio dijalankan, maka objek tersebut tidak dapat ditemukan. Solusinya adalah mengubah *id* tersebut dengan atribut objek yang sama namun memiliki value tetap. Caranya adalah dengan melakukan inspect element pada browser. Objek akan memiliki atribut sebagai berikut

```
<input autocomplete="off" style="z-index:auto" type="text" class="form-control datepicker ng-pristine ng-valid hasDatepicker ng-touched" disableselector="sigma.datepicker.Voyage Date" placeholder="Voyage Date" id="dp1555967983853">
```

Disini kita pilih
```
disableselector="sigma.datepicker.Voyage Date"
```
Karena atribut ini valuenya tetap.

Ubah *xpath:attributes* dengan value]
```katalon
//input[@id='xxx']
```
menjadi
```katalon
//input[@disableselector='sigma.datepicker.Voyage Date']
```

Ubah juga value selector editor dengan
```katalon
//input[@disableselector='sigma.datepicker.Voyage Date']
```

4. Diisi value sesuai dengan kebutuhan pada *test case > input*

![Image 4](img/4.png)

5. Selesai dan *run* katalon untuk mengetahui berfungsi atau masih ada *error*
