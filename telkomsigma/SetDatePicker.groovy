package telkomsigmaAngular
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords

import internal.GlobalVariable

import MobileBuiltInKeywords as Mobile
import WSBuiltInKeywords as WS
import WebUiBuiltInKeywords as WebUI

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import java.lang.String
import java.lang.CharSequence

class SetDatePicker {

	@Keyword
	def setDate(TestObject to, String tanggal, String bulan, String tahun) {

		String xpath_tahun ='//select[contains(@class,\'ui-datepicker-year\') and @data-handler = \'selectYear\']';
		String xpath_bulan = '//select[contains(@class,\'ui-datepicker-month\') and @data-handler = \'selectMonth\']';
		String xpath_tanggal = '//a[contains(@class, \'ui-state-default\') and @href = \'#\']';
		String xpath_datePicker = '//div[(@id = \'ui-datepicker-div\') and contains(@style, \'display: block\')]'
		//String xpath_datePicker2 = '//*[(@id = \'ui-datepicker-div\') and contains(@style, \'display: block\')]'

		try {
			WebUI.click(to);
			KeywordUtil.logInfo("Click tombol date picker");

			TestObject toDatePicker = new TestObject('Thodo_Datepicker')
			toDatePicker.addProperty('xpath', ConditionType.EQUALS, xpath_datePicker)

			String styleDatePicker = WebUI.getAttribute(toDatePicker, 'style');
			CharSequence displayBlock = "display: block";
			boolean isDatePickerOpen = 	styleDatePicker.contains(displayBlock)

			if (isDatePickerOpen == true) {

				KeywordUtil.logInfo("Datepicker is opened");

				TestObject toTahun = new TestObject('Datepicker_year');
				toTahun.addProperty('xpath', ConditionType.EQUALS, xpath_tahun)

				TestObject toBulan = new TestObject('Datepicker_month');
				toBulan.addProperty('xpath', ConditionType.EQUALS, xpath_bulan)

				TestObject toTanggal = new TestObject('Datepicker_date');
				toTanggal.addProperty('xpath', ConditionType.EQUALS, xpath_tanggal)
				toTanggal.addProperty('text', ConditionType.EQUALS, tanggal.toString())

				try {
					WebUI.selectOptionByValue(toTahun, tahun, false)
					KeywordUtil.logInfo("Selecting Year/ Memilih tahun: "+tahun);
					KeywordUtil.markPassed("Datepicker Tahun is successfully set");

					//WebUI.selectOptionByValue(toTahun, tahun.toString(), false)
					WebUI.selectOptionByIndex(toBulan, Integer.parseInt(bulan)-1)
					KeywordUtil.logInfo("Selecting Month/ Memilih bulan: "+bulan);
					KeywordUtil.markPassed("Datepicker Bulan is successfully set");

					WebUI.click(toTanggal);
					KeywordUtil.logInfo("Selecting Date/ Memilih tanggal: "+tanggal);
					KeywordUtil.markPassed("Datepicker Tanggal is successfully set");
				}

				catch (WebElementNotFoundException e1) {
					KeywordUtil.markFailed(e1.detailMessage());
					KeywordUtil.markFailed(e1.CAUSE_CAPTION);
				}

				catch (Exception e2) {
					KeywordUtil.markFailed(e2.detailMessage());
					KeywordUtil.markFailed(e2.CAUSE_CAPTION);
				}
			}
			else {
				KeywordUtil.logInfo("Datepicker is closed or doesn't exist");
			}
		}
		catch (WebElementNotFoundException e) {
			KeywordUtil.markFailed("Element datepicker tidak ditemukan")
		}
		catch (Exception e) {
			KeywordUtil.markFailed("Gagal untuk klik tombol datepicker");
			KeywordUtil.markFailed(e.detailMessage());
			KeywordUtil.markFailed(e.CAUSE_CAPTION);
			KeywordUtil.markFailed(e);
		}
	}
}