package telkomsigmaAngular
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords

import internal.GlobalVariable

import MobileBuiltInKeywords as Mobile
import WSBuiltInKeywords as WS
import WebUiBuiltInKeywords as WebUI

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import java.lang.String

import com.kms.katalon.core.testobject.SelectorMethod

class SetCheckboxToggle {

	@Keyword
	def setCheckbox(TestObject to, boolean checkbox_sts) {
		try {
			String xpath_id = WebUI.getAttribute(to, "for")
			KeywordUtil.logInfo("xpath id adalah = " + xpath_id)

			TestObject to_toggle = new TestObject ('TestObj_Toggle')
			String xpath_toggle_value = '//input[(@type=\'checkbox\') and (@id=\'' + xpath_id + '\')]'
			KeywordUtil.logInfo("Xpath Toggle adalah = " + xpath_toggle_value)

			to_toggle.addProperty('xpath', ConditionType.EQUALS, xpath_toggle_value)
			//to_toggle.addXpath('id', ConditionType.EQUALS, xpath_id)
			//to_toggle.setSelectorValue(SelectorMethod.XPATH, xpath_toggle_value)

			String ng_reflect_model= WebUI.getAttribute(to_toggle, "ng-reflect-model")
			KeywordUtil.logInfo("Value dari Checkbox: " + ng_reflect_model)

			try {

				while ((checkbox_sts == true && (ng_reflect_model == "false" || ng_reflect_model ==""))||(checkbox_sts == false) && (ng_reflect_model == "true")) {
					WebUI.click(to)
					ng_reflect_model = WebUI.getAttribute(to_toggle, "ng-reflect-model")
					KeywordUtil.logInfo("Value dari Checkbox: " + ng_reflect_model)
				}

			}
			catch (WebElementNotFoundException e) {
				KeywordUtil.markFailed(e)
			}

			catch (Exception e) {
				KeywordUtil.markFailed(e)
			}

		} catch (WebElementNotFoundException e) {
			KeywordUtil.markFailed("Element not found woy")
		} catch (Exception e) {
			KeywordUtil.markFailed("Fail to click on element woy")
		}
	}
}